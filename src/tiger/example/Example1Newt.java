/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tiger.example;

import java.io.InputStream;
import javax.media.opengl.GL;
import scene.Scene;
import mesh.loaders.ObjLoader;
import scene.surface.mesh.Mesh;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.WindowNewt;

/**
 *
 * @author cmolikl
 */
public class Example1Newt {

    public static void main(String[] args) {
        
        ObjLoader loader = new ObjLoader();
        Scene<Mesh> scene = loader.loadFile("C:/Users/cmolikl/Projects/Data/cow_triangles.obj");
        
        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1f, 1f, 1f, 1f);
        rs.enable(GL.GL_DEPTH_TEST);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong2.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass = new Pass(vertexStream, fragmentStream);
        pass.scene = scene;
        pass.renderState = rs;
        
        //Camera camera = new Camera(pass, scene);

        WindowNewt w = new WindowNewt(scene, 500, 500);
        w.setEffect(pass);
        w.runFastAsPossible = true;
        w.debug = true;
        w.start();
    }
}
