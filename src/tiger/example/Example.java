/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tiger.example;

import java.io.InputStream;
import scene.Scene;
import mesh.loaders.ObjLoader;
import scene.surface.mesh.Mesh;
import tiger.core.Camera;
import tiger.core.Pass;
import tiger.core.RenderState;
import tiger.core.Window;

import javax.media.opengl.GL;

/**
 *
 * @author cmolikl
 */
public class Example {

    public static void main(String[] args) {
        
        ObjLoader loader = new ObjLoader();
        Scene<Mesh> scene = loader.loadFile("D:/Projects/Data/cow_triangles.obj");
        for(Mesh m : scene.getAllMeshes()) {
            m.renderMethod = Mesh.VERTEX_BUFFER;
        }
        
        RenderState rs = new RenderState();
        rs.clearBuffers(true);
        rs.setClearColor(1f, 1f, 1f, 1f);
        rs.enable(GL.GL_DEPTH_TEST);

        InputStream vertexStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.vert");
        InputStream fragmentStream = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag");
        Pass pass = new Pass(vertexStream, fragmentStream);
        pass.scene = scene;
        pass.renderState = rs;
        
        //Camera camera = new Camera(pass, scene);

        Window w = new Window(scene, 512, 512);
        w.setEffect(pass);
        w.runFastAsPosible = true;
        w.printFps = true;
        w.start();
    }
}
