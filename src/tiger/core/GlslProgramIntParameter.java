/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tiger.core;

import java.io.BufferedReader;
import java.io.IOException;
import javax.media.opengl.GL;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author cmolikl
 */
public class GlslProgramIntParameter extends GlslProgramParameter {
    int value;
    
    public GlslProgramIntParameter(String name, int value) {
        super(name);
        this.value = value;
    }
    
    protected void initValue(GL gl, int location) {
       gl.getGL2().glUniform1i(location, value);
    }

    public int getValue() {
        return value;
    }
    
    public void setValue(int value) {
        this.value = value;
        ChangeEvent e = new ChangeEvent(this);
        for(ChangeListener l : listeners) {
            l.stateChanged(e);
        }
    }
    
    public boolean parseValue(String string) {
       try {
           setValue(Integer.parseInt(string));
       }
       catch(Exception e) {
           return false;
       }
       return true;
    }
    
    public String toString() {
        return "" + value;
    }
}
