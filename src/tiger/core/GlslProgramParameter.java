package tiger.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashSet;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import javax.swing.event.ChangeListener;

public abstract class GlslProgramParameter {
    protected HashSet<ChangeListener> listeners = new HashSet<>();
    public String name;
    
    protected GlslProgramParameter(String name) {
        this.name = name;
    }
    
    public void init(GlslProgram glslProgram) {
            GL2 gl = GLU.getCurrentGL().getGL2();
            int location = gl.glGetUniformLocation(glslProgram.getGlNumber(), name);
            initValue(gl, location);
    }
    
    protected abstract void initValue(GL gl, int location);

    public void addChangeListener(ChangeListener l) {
        listeners.add(l);
    }
    
    public abstract boolean parseValue(String s);
    
    @Override
    public abstract String toString();

}
