/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tiger.core;

import java.awt.Toolkit;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

/**
 *
 * @author cmolikl
 */
public class ViewPort implements GLEventListener {
    
    private float x;
    private float y;
    private float width;
    private float height;
    private float dpiScale = 1f;

    private int windowWidth;
    private int windowHeight;

    private GLEventListener listener;

    public ViewPort(float x, float y, float width, float height, GLEventListener listener) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.listener = listener;
        int dpi = Toolkit.getDefaultToolkit().getScreenResolution();
        String version = System.getProperty("java.version");
        if(!version.startsWith("1.")) {
            this.dpiScale = dpi/96.0f;
        }
    }

    public void init(GLAutoDrawable drawable) {
        listener.init(drawable);
    }

    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        //int[] viewport = new int[4];
        //gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 4);
        gl.glPushAttrib(GL.GL_VIEWPORT);
        gl.glViewport((int) (x * windowWidth), (int) (y * windowHeight), (int) (width * windowWidth), (int) (height * windowHeight));
        listener.display(drawable);
        //gl.glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
        gl.glPopAttrib();
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        windowWidth = (int)(width * dpiScale);
        windowHeight = (int)(height * dpiScale);
        if(listener != null) listener.reshape(drawable, x, y, (int)(this.width * windowWidth), (int)(this.height * windowHeight));
    }

    public void displayChanged(GLAutoDrawable drawable, boolean bln, boolean bln1) {}

    public void dispose(GLAutoDrawable arg0) {
    }

}
