/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiger.core;

import gleem.MouseButtonHelper;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import tiger.recording.ImageWriter;

/**
 *
 * @author cmolikl
 */
public class WindowGlListener implements GLEventListener {
    
    Window w;
   
    int frame = 0;
    
    WindowGlListener(Window w) {
        this.w = w;
    } 
    
    public void init(GLAutoDrawable drawable) {
        //drawable.setGL(new DebugGL(drawable.getGL()));
        //if(effect != null) {
        
        if(!w.viewPorts.isEmpty()) {
            GL gl = drawable.getGL();

            if(w.debug) {
                w.setDebug();
            }
        
            //effect.init(drawable);
            for(ViewPort viewPort : w.viewPorts) {
                viewPort.init(drawable);
            }
           
            // Register the window with the ManipManager
            //ManipManager manager = ManipManager.getManipManager();
            //manager.registerWindow(drawable);
            w.initScreenShotBuffer(drawable);
        }
        
        if(w.bSphereProvider != null && w.viewer == null) {
            w.viewer = new OrthogonalExaminerViewer(MouseButtonHelper.numMouseButtons());
            //viewer = new ExaminerViewer(MouseButtonHelper.numMouseButtons());
            w.viewer.attach(w.canvas, w.bSphereProvider);
            w.viewer.setNoAltKeyMode(true);
            if(w.runFastAsPosible) {
                w.viewer.setAutoRedrawMode(false);
            }
            else {
                w.viewer.setAutoRedrawMode(true);
            }
            w.viewer.rotateFaster();
            w.viewer.viewAll(w.canvas.getGL());
        }
    }
    
    public void display(GLAutoDrawable drawable) {

        if(!w.viewPorts.isEmpty()) {
            GL2 gl = drawable.getGL().getGL2();

            if(w.viewer != null && w.interaction) {
                synchronized(w.viewer) {
                    w.viewer.update(gl);
                }
            }

            for(ViewPort viewPort : w.viewPorts) {
                viewPort.display(drawable);
            }
            
            if(w.captureScreenShot.getValue() != 0 || w.recordVideo.getValue() != 0) {
                w.screenShot = w.getScreenShot(drawable);
            }
            
            if(w.captureScreenShot.getValue() != 0) {
                w.captureScreenShot.setValue(0);
                ImageWriter imageWriter = new ImageWriter(w.screenShot, w.screenShotPath + "screenShot" + frame + ".png");
                w.screenShot = new BufferedImage(w.screenShot.getWidth(), w.screenShot.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
                imageWriter.start();
                frame++;
            }
            
            if(w.recordVideo.getValue() != 0 && w.videoWriter != null) {
                w.videoWriter.addFrame(w.screenShot);
//                byte[] pixels = ((DataBufferByte) w.screenShot.getRaster().getDataBuffer()).getData();
//                Mat frameMat = new Mat(w.frameSize, CvType.CV_8UC3);
//                frameMat.put(0, 0, pixels);
//                w.videoWriter.write(frameMat);
            }
            
            if(w.printFps) {
                w.fps++;
                long time = System.currentTimeMillis();
                if (time - w.prevTime >= 1000) {
                    w.currentFps = (float) w.fps;
                    System.out.println("FPS: " + w.currentFps);
                    w.prevTime = time;
                    w.fps = 0;
                }
            }

            if(false) {
                w.viewer.rotateAboutFocalPoint(w.yAxisRotationStep);
            }
        }
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        /*if(effect != null) {
            effect.reshape(drawable, x, y, width, height);
        }*/
        if(!w.viewPorts.isEmpty()) {
            for(ViewPort viewPort : w.viewPorts) {
                viewPort.reshape(drawable, x, y, width, height);
            }
        }
        w.initScreenShotBuffer(drawable);
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
    }
}
