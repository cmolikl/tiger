/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tiger.core;

import java.io.BufferedReader;
import java.io.IOException;
import javax.media.opengl.GL;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author cmolikl
 */
public class GlslProgramUIntParameter extends GlslProgramIntParameter {
    
    public GlslProgramUIntParameter(String name, int value) {
        super(name, value);
    }
    
    protected void initValue(GL gl, int location) {
       gl.getGL2().glUniform1ui(location, value);
    }
}
