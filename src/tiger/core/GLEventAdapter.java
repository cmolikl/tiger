/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tiger.core;

import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

/**
 *
 * @author cmolikl
 */
public class GLEventAdapter implements GLEventListener {

    public void init(GLAutoDrawable glad) {}

    public void display(GLAutoDrawable glad) {}

    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {}

    public void displayChanged(GLAutoDrawable glad, boolean bln, boolean bln1) {}

    public void dispose(GLAutoDrawable glad) {
    }

}
