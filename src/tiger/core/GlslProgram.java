/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tiger.core;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

/**
 *
 * @author cmolikl
 */
public class GlslProgram {

    private int glNumber;

    public int getGlNumber() {
        return glNumber;
    }

    public void init(GL gl) {
        glNumber = gl.getGL2().glCreateProgram();
    }

    public void attachGlslShader(GL gl, GlslShader shader) {
        GL2 gl2 = gl.getGL2();
        gl2.glAttachShader(glNumber, shader.getGlNumber());
        if (shader instanceof GlslGeometryShader) {
            GlslGeometryShader gs = (GlslGeometryShader) shader;
            gl2.glProgramParameteriARB(glNumber, GL2.GL_GEOMETRY_INPUT_TYPE_ARB, gs.getInputType());
            gl2.glProgramParameteriARB(glNumber, GL2.GL_GEOMETRY_OUTPUT_TYPE_ARB, gs.getOutputType());
            int[] temp = new int[1];
            gl2.glGetIntegerv(GL2.GL_MAX_GEOMETRY_OUTPUT_VERTICES_ARB, temp, 0);
            gl2.glProgramParameteriARB(glNumber, GL2.GL_GEOMETRY_VERTICES_OUT_ARB, temp[0]);
        }
    }

    public void detachGlslShader(GL gl, GlslShader shader) {
        gl.getGL2().glDetachShader(glNumber, shader.getGlNumber());
    }

    public void linkProgram(GL gl) {
        GL2 gl2 = gl.getGL2();
        gl2.glLinkProgram(glNumber);
        int[] tmp = new int[1];
        gl2.glGetProgramiv(glNumber, GL2.GL_LINK_STATUS, tmp, 0);
        if (tmp[0] == 0) {
            System.out.println("GLSL program failed to link:");
            byte[] buffer = new byte[1000];
            gl2.glGetProgramInfoLog(glNumber, 1000, (int[]) null, 0, buffer, 0);
            System.out.println("Error message: \"" + new String(buffer) + "\"");
        }
    }

    public void useProgram(GL gl) {
        gl.getGL2().glUseProgram(glNumber);
    }
}
