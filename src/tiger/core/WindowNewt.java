/*
 * BasicJoglListener.java
 *
 * Created on 21.10.2007, 14:16:18
 *
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tiger.core;

//import com.jogamp.opengl.util.Animator;
import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.opengl.GLWindow;
import gleem.BSphereProvider;
import gleem.ExaminerViewer;
import gleem.MouseButtonHelper;
import javax.media.nativewindow.WindowClosingProtocol;
import javax.media.opengl.DebugGL2;
import javax.media.opengl.DebugGL3;
import javax.media.opengl.DebugGL3bc;
import javax.media.opengl.DebugGL4;
import javax.media.opengl.DebugGL4bc;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL3;
import javax.media.opengl.GL3bc;
import javax.media.opengl.GL4;
import javax.media.opengl.GL4bc;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import tiger.animation.AnimatorNewt;

/**
 *
 * @author cmolikl
 */
public class WindowNewt implements GLEventListener {
    
    private String cameraPath = "C:/Temp/camera.txt";

    protected ExaminerViewer viewer;
    protected BSphereProvider bSphereProvider;
    protected AnimatorNewt animator;
    protected GLEventListener effect;
    public GLWindow canvas;

    public boolean runFastAsPossible = false;
    public boolean debug = false;

    KeyListener keyListener = null;
    
    public WindowNewt(BSphereProvider bSphereProvider) {
        this.bSphereProvider = bSphereProvider;
        GLCapabilities capabilities = new GLCapabilities(GLProfile.getDefault());
        capabilities.setSampleBuffers(true);
        capabilities.setNumSamples(4);
        capabilities.setDoubleBuffered(true);
        canvas = GLWindow.create(capabilities);
        canvas.setSize(512, 512);
    }

    public WindowNewt(BSphereProvider bSphereProvider, boolean debug) {
        this(bSphereProvider);
        this.debug = debug;
    }

    public WindowNewt(int width, int height) {
        GLCapabilities capabilities = new GLCapabilities(GLProfile.getDefault());
        capabilities.setSampleBuffers(true);
        capabilities.setNumSamples(4);
        capabilities.setDoubleBuffered(true);
        canvas = GLWindow.create(capabilities);
        canvas.setSize(width, height);
    }
    
    public WindowNewt(BSphereProvider bSphereProvider, int width, int height) {
        this(width, height);
        this.bSphereProvider = bSphereProvider;
    }

    public WindowNewt(BSphereProvider bSphereProvider, int width, int height, boolean debug) {
        this(bSphereProvider, width, height);
        this.debug = debug;
    }

    private void setDebug() {
        GL gl = canvas.getGL();
        if (gl.isGL4bc()) {
            final GL4bc gl4bc = gl.getGL4bc();
            canvas.setGL(new DebugGL4bc(gl4bc));
        }
        else {
            if (gl.isGL4()) {
                final GL4 gl4 = gl.getGL4();
                canvas.setGL(new DebugGL4(gl4));
            }
            else {
                  if (gl.isGL3bc()) {
                      final GL3bc gl3bc = gl.getGL3bc();
                      canvas.setGL(new DebugGL3bc(gl3bc));
                  }
                  else {
                      if (gl.isGL3()) {
                          final GL3 gl3 = gl.getGL3();
                          canvas.setGL(new DebugGL3(gl3));
                      }
                      else {
                          if (gl.isGL2()) {
                              final GL2 gl2 = gl.getGL2();
                              canvas.setGL(new DebugGL2(gl2));
                          }
                      }
                  }
             }
        }
    }
    
    public ExaminerViewer getControler() {
        return viewer;
    }

    public void setControler(ExaminerViewer viewer) {
        this.viewer = viewer;
    }
    
    public void setAnimator(AnimatorNewt animator) {
        this.animator = animator;
    }
    
    public void setKeyListener(KeyListener listener) {
        keyListener = listener;
    }
    
    public void setEffect(GLEventListener effect) {
        this.effect = effect;
    }
    
    public void init(GLAutoDrawable drawable) {
        if(effect != null) {
            GL gl = drawable.getGL();

            if(debug) {
                setDebug();
            }
           
            // Register the window with the ManipManager
            //ManipManager manager = ManipManager.getManipManager();
            //manager.registerWindow(drawable);
            if(bSphereProvider != null && viewer == null) {
                viewer = new OrthogonalExaminerViewer(MouseButtonHelper.numMouseButtons());
                //viewer = new ExaminerViewer(MouseButtonHelper.numMouseButtons());
                viewer.attach(drawable, bSphereProvider);
                viewer.setNoAltKeyMode(true);
                if(runFastAsPossible) {
                    viewer.setAutoRedrawMode(false);
                }
                else {
                    viewer.setAutoRedrawMode(true);
                }
                viewer.rotateFaster();
                viewer.viewAll(gl);
            }
        }
    }

    public void display(GLAutoDrawable drawable) {
        if(effect != null) {
            GL2 gl = drawable.getGL().getGL2();
            if(viewer != null) {
                synchronized(viewer) {
                    viewer.update(gl);
                }
            }
            
            effect.display(drawable);

//            if(rotateAroundYAxis) {
//                viewer.rotateAboutFocalPoint(yAxisRotationStep);
//            }
        }
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        if(effect != null) {
            effect.reshape(drawable, x, y, width, height);
        }
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }


    public void build() {
        canvas.addGLEventListener(this);
        if(keyListener != null) {
            canvas.addKeyListener(keyListener);
        }
        canvas.setDefaultCloseOperation(WindowClosingProtocol.WindowClosingMode.DISPOSE_ON_CLOSE);
        canvas.setVisible(true);
    }

    public void run() {
        canvas.display();
    }

    /**
     * @param args the command line arguments
     */
    public void start() {
        build();
        if(runFastAsPossible) {
            if(animator == null) animator = new AnimatorNewt();
            animator.addCanvas(canvas);
            animator.setRunAsFastAsPossible(true);
            //animator.setUpdateOnlyOnChange(false);
            animator.start();
        }
        else run();
    }

    public void dispose(GLAutoDrawable arg0) {
        effect.dispose(arg0);
        System.exit(0);
    }
}


