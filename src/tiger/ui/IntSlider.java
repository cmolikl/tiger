/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tiger.ui;

import java.util.Dictionary;
import java.util.Hashtable;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import tiger.core.GlslProgramFloatParameter;
import tiger.core.GlslProgramIntParameter;

/**
 *
 * @author cmolikl
 */
public class IntSlider extends JSlider {

    private class ParamChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent e) {
            GlslProgramIntParameter p = (GlslProgramIntParameter) e.getSource();
            setValue(p.getValue());
        }
    }

    private int defaultMajorTickSpacing = 25;
    public float min;
    public float max;
    
    public IntSlider(GlslProgramIntParameter param, int orientation, int min, int max) {
        super(orientation, min, max, param.getValue());
        setMajorTickSpacing(max);
        setMinorTickSpacing(1);
        setPaintTicks(true);
        setPaintLabels(true);

        //this.min = min;
        //this.max = max;
        addChangeListener(new IntSliderListener(param));
        param.addChangeListener(new ParamChangeListener());
    }
}
