/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiger.recording;


import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;

/**
 *
 * @author cmolikl
 */
public class VideoWriterOCV extends Thread {
    
    private static boolean libraryLoaded = false;
    
    String videoPath = "c:/Temp/screenGrab.mp4";
    
    private LinkedList<Mat> framesToWrite;
    
    private org.opencv.videoio.VideoWriter writer;
    private Size size;
    private int frameRate;
    //private long frameTime;
    //private int frameRepetition = 1;
    
    private boolean recording = false;
    
    
    public VideoWriterOCV(int width, int height, int frameRate) {
        this.size = new Size(width, height);
        this.frameRate = frameRate;
        //this.frameTime = 1000 / frameRate;
        framesToWrite = new LinkedList<>();
    }
    
    /*public VideoWriterOCV(int width, int height, int frameRate, int frameRepetition) {
        this(width, height, frameRate);
        this.frameRepetition = frameRepetition;
    }*/
    
    public VideoWriterOCV(int width, int height, int frameRate, String path) {
        this(width, height, frameRate);
        this.videoPath = path;
    }
    
    @Override
    public void run() {
        if(!libraryLoaded) {
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            libraryLoaded = true;
        }
        
        if(!recording) {
            writer = new org.opencv.videoio.VideoWriter(videoPath, org.opencv.videoio.VideoWriter.fourcc('x', '2','6','4'), frameRate, size, true);
            recording = true;
        }
        
        //long frameTimeInVideo = 0;
        int framesWritten = 0;
        while(recording) {
            if(!framesToWrite.isEmpty()) {
                while(!framesToWrite.isEmpty()) {
                    Mat frame = framesToWrite.removeFirst();
                    writer.write(frame);
                    framesWritten++;
                }
            }
            else {
                Thread.yield();
            }
        }
        
        writer.release();
        System.out.println("VideoWriter has written " + framesWritten + " frames.");
    }
    
    public synchronized void addFrame(BufferedImage frame) {
        if(recording) {
            byte[] pixels = ((DataBufferByte) frame.getRaster().getDataBuffer()).getData();
            Mat frameMat = new Mat(size, CvType.CV_8UC3);
            frameMat.put(0, 0, pixels);
            framesToWrite.addLast(frameMat);
        }
    }
    
    public void stopRecording() {
        recording = false;
    }
    
    public boolean isRecording() {
        return recording;
    }
}
