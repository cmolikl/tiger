/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiger.recording;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author cmolikl
 */
public class VideoWriter extends Thread {
    
    String videoPath = "c:/Temp/screenGrab.mp4";
    
    private LinkedList<BufferedImage> framesToWrite;
    
    private IMediaWriter writer;
    private int width;
    private int height;
    private long frameTime;
    private int frameRepetition = 1;
    
    private boolean recording = false;
    
    
    public VideoWriter(int width, int height, int frameRate) {
        this.width = width;
        this.height = height;
        this.frameTime = 1000 / frameRate;
        framesToWrite = new LinkedList<>();
    }
    
    public VideoWriter(int width, int height, int frameRate, int frameRepetition) {
        this(width, height, frameRate);
        this.frameRepetition = frameRepetition;
    }
    
    public VideoWriter(int width, int height, int frameRate, int frameRepetition, String path) {
        this(width, height, frameRate, frameRepetition);
        this.videoPath = path;
    }
    
    @Override
    public void run() {
        if(!recording) {
            writer = ToolFactory.makeWriter(videoPath);
            writer.addVideoStream(0, 0, ICodec.ID.CODEC_ID_MPEG2VIDEO, width, height);
            recording = true;
        }
        
        long frameTimeInVideo = 0;
        
        while(recording) {
            if(framesToWrite.size() > 0) {
                while(framesToWrite.size() > 0) {
                    BufferedImage frame = framesToWrite.removeFirst();
                    for(int i = 0; i < frameRepetition; i++) {
                        writer.encodeVideo(0, frame, frameTimeInVideo, TimeUnit.MILLISECONDS);
                        frameTimeInVideo += frameTime;
                    }
                }
            }
            else {
                Thread.yield();
            }
        }
        
        writer.close();
    }
    
    public synchronized void addFrame(BufferedImage frame) {
        if(recording) {
            framesToWrite.addLast(frame);
        }
    }
    
    public void stopRecording() {
        recording = false;
    }
    
    public boolean isRecording() {
        return recording;
    }
}