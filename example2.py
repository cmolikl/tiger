from tiger.core import Pass, RenderState, GLWindow
from javax.media.opengl import GL, GLCapabilities
from java.lang import *

rs1 = RenderState()
rs1.clearBuffers(True)
rs1.enable(GL.GL_DEPTH_TEST)

vs = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.vert")
fs = ClassLoader.getSystemResourceAsStream("tiger/example/Phong.frag")
p1 = Pass(vs, fs)
p1.renderState = rs1

capabilities = GLCapabilities()
capabilities.setSampleBuffers(True)
capabilities.setNumSamples(8)
capabilities.setDoubleBuffered(True)

w = GLWindow(capabilities)
w.setScene(None)
w.setEffect(p1)
w.pack()
w.setVisible(True)

